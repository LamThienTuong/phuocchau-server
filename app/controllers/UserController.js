const UserService = require('../services/UserService')

class UserController {
  async login(req, res) {
    try {
      const { email, password } = req.body
      const response = await UserService.login({ email, password })
      if (response) {
        return res.status(200).json(response)
      }
      return res.status(404).json({ message: 'LOGIN_FAIL' })
    } catch (error) {
      throw error
    }
  }
}

module.exports = new UserController()
