const ItemService = require('../services/ItemService')

class ItemComtroller {
  async createItemCategory(req, res) {
    try {
      const { name } = req.body
      const response = await ItemService.createItemCategoryService(name)
    } catch (error) {
      throw error
    }
  }
}

module.exports = new ItemComtroller()
