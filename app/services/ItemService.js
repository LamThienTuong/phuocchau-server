const Item = require('../models/items')
const ItemCategory = require('../models/itemCategory')

class ItemService {
  constructor(model) {
    this.createItemCategoryService = createItemCategoryService.login.bind(this)
  }

  async createItemCategoryService(name) {
    try {
      const newItemCategory = new ItemCategory(name)
      newItemCategory.created = new Date()
      await newItemCategory.save()
      return newItemCategory
    } catch (error) {
      throw error
    }
  }
}

module.exports = new ItemService()
