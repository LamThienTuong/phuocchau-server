const User = require('../models/user')

class UserService {
  constructor(model) {
    this.login = this.login.bind(this)
  }

  async login({ email, password }) {
    try {
      const user = await User.findOne({ email })
      if (user) {
      } else {
        return false
      }
    } catch (error) {
      throw error
    }
  }
}

module.exports = new UserService(User)
