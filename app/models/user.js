const mongoose = require('./base')

const User = mongoose.model('user', {
  email: String,
  password: String,
  socialId: String,
  socialType: String,
  verified: {
    type: Boolean,
    defaule: false,
  },
  token: String,
  created: Date,
})

module.exports = User
