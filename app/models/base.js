const mongoose = require('mongoose')

mongoose.connect(process.env.DB || 'mongodb://localhost:27017/phuoc-chau', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})

mongoose.Promise = Promise

module.exports = mongoose
