const mongoose = require('./base')

const ItemCategory = mongoose.model('itemCategory', {
  name: String,
  created: Date,
  update: Date,
})

module.exports = ItemCategory
