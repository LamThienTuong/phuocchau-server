const mongoose = require('./base')

const Item = mongoose.model('item', {
  itemId: String,
  title: String,
  description: String,
  category: {
    type: mongoose.Types.ObjectId,
    ref: 'itemCategory',
  },
  color: {
    type: mongoose.Types.ObjectId,
    ref: 'itemColor',
  },
  fabricType: {
    type: mongoose.Types.ObjectId,
    ref: 'itemFabric',
  },
  sewingStyle: String,
  patternStyle: String,
  created: Date,
  updated: Date,
})

module.exports = Item
