module.exports={
    // Error
    INTERNAL_SERVER_ERROR: 'Something wrong with server. Internal server error',
    MISSING_REQUIRE: 'Missing require field!',
    NOT_EXISTS: 'Record is not exists',
    // Success
    CREATE_SUCCESS: 'Create new record success'
}
