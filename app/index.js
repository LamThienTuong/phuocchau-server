const express = require('express')
const bodyParser = require('body-parser')
const dotenv = require('dotenv')
const cors = require('cors')
const morgan = require('morgan')
const route = require('./routes')

dotenv.config()

class App {
  constructor() {
    this.app = express()
    this._setupMiddleware()
    this._setupRoute()
  }

  _setupMiddleware() {
    this.app.use(cors())
    this.app.use(bodyParser.json())
    this.app.use(morgan('short'))
  }

  _setupRoute() {
    this.app.use(route)
  }

  start() {
    const PORT = process.env.PORT || 8008
    this.app.listen(PORT, () => {
      console.log('App listen on port ' + PORT)
    })
  }
}

module.exports = App
