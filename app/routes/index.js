const Base = require('./base')
const api = require('./api')

class IndexRoute extends Base {
  _setupRoute() {
    this.router.use('/api/v1', api)
  }
}

module.exports = new IndexRoute().router
