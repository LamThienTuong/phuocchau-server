const Base = require('../base')
const user = require('./user')
const item = require('./item')

class ApiRoute extends Base {
  _setupRoute() {
    this.router.use('/user', user)
    this.router.use('/item', item)
  }
}

module.exports = new ApiRoute().router
