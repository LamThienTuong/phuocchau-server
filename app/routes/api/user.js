const Base = require('../base')
const UserController = require('../../controllers/UserController')

class UserRoute extends Base {
  _setupRoute() {
    this.router.post('/login', this._handleRoute(UserController.login))
  }
}

module.exports = new UserRoute().router
