const Base = require('../base')
const ItemController = require('../../controllers/ItemController')

class ItemRouter extends Base {
  _setupRoute() {
    this.router.post(
      '/category',
      this._handleRoute(ItemController.createItemCategory)
    )
  }
}

module.exports = new ItemRouter().router
