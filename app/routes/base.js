const Router = require('express').Router

class BaseRouter {
  constructor() {
    this.router = Router()
    this._setupRoute && this._setupRoute()
  }

  _handleRoute(handler) {
    return (req, res, next) => {
      try {
        handler(req, res)
      } catch (error) {
        res.status(500).json()
      }
    }
  }
}

module.exports = BaseRouter
